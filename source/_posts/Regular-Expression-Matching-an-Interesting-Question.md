---
title: 'Regular Expression Matching, an Interesting Question'
date: 2017-01-21 23:00:27
tags:
    - Regular Expression
    - Algorithms
---

## Problem

The problem is simple: **Implement regular expression matching with support for ‘.’ and ‘*’**. The detailed description is as follows.


        ‘.’ Matches any single character.
        ‘*’ Matches zero or more of the preceding element.
        The matching should cover the entire input string (not partial).

        The function prototype should be:
        bool isMatch(const char *s, const char *p)

        Some examples:
        isMatch(“aa”,”a”) → false
        isMatch(“aa”,”aa”) → true
        isMatch(“aaa”,”aa”) → false
        isMatch(“aa”, “a*”) → true
        isMatch(“aa”, “.*”) → true
        isMatch(“ab”, “.*”) → true
        isMatch(“aab”, “c*a*b”) → true


To do this, I followed an intuitive method to iterate through both strings and greedily match "\*", which seems to work just fine if we consider cases like:

    "abc", "a.*c"
    "aa", ".*"
    "aac", "a.*"
    "cba", "b*c*.*"

Here we just match everything we can find to "\*". For example, in the fourth example, the first part "b\*" will not be matched, "c*" will match the first "c" in "cbd", the ".*" will match "ba". This approach is wrong because it's unable to catch cases like below.

    "abc", "a.*b.*c.*"

If we follow the greedy approach, the second ".*" will match everything after the first "a" in "abc", i.e., "bc". After that, the "b.*c.*" will have nothing to match and will report this case as `false`. In principal, a greedy match will not work because it is impossible to know how many characters to match until, of course, you try it. Therefore, the correct method should somehow exhaustively search through all possible number of matches for each "\*", which naturally leads us to a **recursive** or **dynamic programming** solution.

## Solution

        ```cpp
            class Solution {
            public:
                bool isMatch(string s, string p) {
                    if (p.empty()) return s.empty();
                    if (p.length() == 1) return (s.length() == 1) && (p[0] == s[0] || p[0] == '.');
                    if (p[1] == '*') return isMatch(s, p.substr(2)) || (!s.empty() && (p[0] == s[0] || p[0] == '.') && isMatch(s.substr(1), p));
                    return !s.empty() && (p[0] == s[0] || p[0] == '.') && isMatch(s.substr(1), p.substr(1));
                }
            };
        ```

This is a four-line c++ recursive solution. The basic idea about the recursion is

        1. If the next character in p is "\*", we are going to to exhaustively search cases where the "\*" matches 0, 1, 2, ... characters in s.
        2. If not, test if current character matches and move to the next one (both s and p).

Even though the overhead of this solution is non-negligible given that it's not tail-recursive, this is a very clean solution in my opinion.

PS: It could be further reduced down to 3 lines if rewritten in C, which is left to the readers as an exercise.
