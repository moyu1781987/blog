---
title: About Me
date: 2017-01-18 22:22:37
---
## Who am I?
I study Computer Science in UCLA as a graduate student.

This site is built to store and share thoughts on interesting technologies.

## For potential recruiters
I am actively seeking for internship opportunity in Summer 2017, my resume could be found [here](/assets/resume.pdf).

## Projects
Some of the projects I've done could be found [here](http://seanxwzhang.github.io/projects/).

## Old sites
If you are interested in what I've done in undergraduate years, it's [here](http://seanxwzhang.github.io/public/old_version/).
